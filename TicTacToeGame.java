package TugasPP;
import java.util.InputMismatchException;
import java.util.Scanner;

	public class TicTacToeGame {
		static Scanner tictac=new Scanner(System.in);
		static void play(){
				int number=-1,player=1,choose;
				char[][]tictactoe=new char[3][3];
				for(int row=0;row<3;row++){
					for(int column=0;column<3;column++){
						tictactoe[row][column]=' ';
					}
				}
				do{
					int choice2 = -1;
					do{
						String choice="a";
						for(int row=0;row<3;row++){
							for(int column=0;column<3;column++){
								tictactoe[row][column]=' ';
							}
						}
						System.out.print("Input character ^o^ ( x / o ) : ");
						try{
							choice=tictac.nextLine();
							if(choice.contentEquals("x") == false && choice.contentEquals("o") == false){
								throw new InputMismatchException();
							}
						}catch(InputMismatchException e){
							System.out.println("-----------------------------------");
							System.out.println("Input 'x' or 'o' ");
						}
						
							if(choice.contentEquals("x")){
								number=1;
								break;
							}
							if(choice.contentEquals("o")){
								number=0;
								break;
							}
							
					}while(true);
					
					while(true){
						int isi=0;
						System.out.println("===================================");
						System.out.println("\tYou turn -"+player);
						System.out.println();
						System.out.println("\t  "+tictactoe[0][0]+" |   "+tictactoe[0][1]+"   | "+tictactoe[0][2]);
						System.out.println("\t____|_______|____");
						System.out.println("\t  "+tictactoe[1][0]+" |   "+tictactoe[1][1]+"   | "+tictactoe[1][2]);
						System.out.println("\t____|_______|____");
						System.out.println("\t  "+tictactoe[2][0]+" |   "+tictactoe[2][1]+"   | "+tictactoe[2][2]);
						System.out.println("\t    |       |    ");
						System.out.println("\n");
						
					
						if(		tictactoe[0][0]!=' ' && tictactoe[0][0]==tictactoe[0][1] && tictactoe[0][0]==tictactoe[0][2]||
								tictactoe[1][0]!=' ' && tictactoe[1][0]==tictactoe[1][1] && tictactoe[1][0]==tictactoe[1][2]||
								tictactoe[2][0]!=' ' && tictactoe[2][0]==tictactoe[2][1] && tictactoe[2][0]==tictactoe[2][2]||
								tictactoe[0][0]!=' ' && tictactoe[0][0]==tictactoe[1][1] && tictactoe[0][0]==tictactoe[2][2]||
								tictactoe[0][2]!=' ' && tictactoe[0][2]==tictactoe[1][1] && tictactoe[0][2]==tictactoe[2][0]||
								tictactoe[0][0]!=' ' && tictactoe[0][0]==tictactoe[1][0] && tictactoe[0][0]==tictactoe[2][0]||
								tictactoe[0][1]!=' ' && tictactoe[0][1]==tictactoe[1][1] && tictactoe[0][1]==tictactoe[2][1]||
								tictactoe[0][2]!=' ' && tictactoe[0][2]==tictactoe[1][2] && tictactoe[0][2]==tictactoe[2][2])
						{
							if(player==1)player++;
							else if(player==2)player--;
							System.out.println("Game Over ! Player-"+player+" WON !!!!");
							do{
								try{
									System.out.print("Type '1' to play again, or type '0' to exit the game : ");
									choice2=tictac.nextInt();
									
									if((choice2==1) == false && (choice2==0) == false){
										throw new InputMismatchException();
									}
								}
								catch(InputMismatchException e){
									System.out.println("------------------------------");
									System.out.println("Input '1' or '0'");
									tictac.nextLine();
									continue;
								}break;
							}while(true);
							break;
						}
						
						
						for(int Row=0;Row<3;Row++){
							for(int Column=0;Column<3;Column++){
								if(tictactoe[Row][Column]!=' '){
									isi++;
								}
							}
						}if (isi==9){
							System.out.println("Draw !! Game Over! ");
							do{
								try{
									System.out.print("Type '1' to play again, and type '0' to exit the game : ");
									choice2=tictac.nextInt();
									
									if((choice2==1) == false && (choice2==0) == false){
										throw new InputMismatchException();
									}
								}
								catch(InputMismatchException e){
									System.out.println("------------------------------");
									System.out.println("Input '1' or '0' ");
									tictac.nextLine();
									continue;
								}break;
							}while(true);
							break;
						}
						
						
						System.out.println("Input number to the empty room :");
						if(tictactoe[0][0]==' '){
							System.out.println("1. Row 1 Column 1");
						}
						if(tictactoe[0][1]==' '){
							System.out.println("2. Row 1 Column 2");
						}
						if(tictactoe[0][2]==' '){
							System.out.println("3. Row 1 Column 3");
						}
						if(tictactoe[1][0]==' '){
							System.out.println("4. Row 2 Column 1");
						}
						if(tictactoe[1][1]==' '){
							System.out.println("5. Row 2 Column 2");
						}
						if(tictactoe[1][2]==' '){
							System.out.println("6. Row 2 Column 3");
						}
						if(tictactoe[2][0]==' '){
							System.out.println("7. Row 3 Column 1");
						}
						if(tictactoe[2][1]==' '){
							System.out.println("8. Row 3 Column 2");
						}
						if(tictactoe[2][2]==' '){
							System.out.println("9. Row 3 Column 3");
						}
						do{
							
							System.out.print(">> ");
							choose=-1;
							try{
								choose=tictac.nextInt();
								break;
							}catch(InputMismatchException e){
								System.out.println("Input Choice");
								tictac.nextLine();
								choose=-1;
								continue;
							}
						}while(true);
						
						switch(choose){
						case 1:
							if(tictactoe[0][0]==' ' && choose==1){
								if(number==1){
									tictactoe[0][0]='x';
									number--;
								}
								else if(number==0){
									tictactoe[0][0]='o';
									number++;
								}
									if(player==1)player++;
									else if(player==2)player--;
							}
							else if(tictactoe[0][0]!=' ' && choose==1){
								System.out.println("Input your choice to the empty room");
							}break;
						case 2:
							if(tictactoe[0][1]==' ' && choose==2){
								if(number==1){
									tictactoe[0][1]='x';
									number--;
								}
								else if(number==0){
									tictactoe[0][1]='o';
									number++;
								}
									if(player==1)player++;
									else if(player==2)player--;
							}
							else if(tictactoe[0][1]!=' ' && choose==2){
								System.out.println("Input your choice to the empty room");
							}break;
						case 3:
							if(tictactoe[0][2]==' ' && choose==3){
								if(number==1){
									tictactoe[0][2]='x';
									number--;
								}
								else if(number==0){
									tictactoe[0][2]='o';
									number++;
								}	
									if(player==1)player++;
									else if(player==2)player--;
							}
							else if(tictactoe[0][2]!=' ' && choose==3){
								System.out.println("Input your choice to the empty room");
							}
						case 4:
							if(tictactoe[1][0]==' ' && choose==4){
								if(number==1){
									tictactoe[1][0]='x';
									number--;
								}
								else if(number==0){
									tictactoe[1][0]='o';
									number++;
								}	
									if(player==1)player++;
									else if(player==2)player--;
							}
							else if(tictactoe[1][0]!=' ' && choose==4){
								System.out.println("Input your choice to the empty room");
							}break;
						case 5:
							if(tictactoe[1][1]==' ' && choose==5){
								if(number==1){
									tictactoe[1][1]='x';
									number--;
								}
								else if(number==0){
									tictactoe[1][1]='o';
									number++;
								}	
									if(player==1)player++;
									else if(player==2)player--;
							}
							else if(tictactoe[1][1]!=' ' && choose==5){
								System.out.println("Input your choice to the empty room");
							}break;
						case 6:
							if(tictactoe[1][2]==' ' && choose==6){
								if(number==1){
									tictactoe[1][2]='x';
									number--;
								}
								else if(number==0){
									tictactoe[1][2]='o';
									number++;
								}	
									if(player==1)player++;
									else if(player==2)player--;
							}
							else if(tictactoe[1][2]!=' ' && choose==6){
								System.out.println("Input your choice to the empty room");
							}break;
						case 7:
							if(tictactoe[2][0]==' ' && choose==7){
								if(number==1){
									tictactoe[2][0]='x';
									number--;
								}
								else if(number==0){
									tictactoe[2][0]='o';
									number++;
								}	
									if(player==1)player++;
									else if(player==2)player--;
							}
							else if(tictactoe[2][0]!=' ' && choose==7){
								System.out.println("Input your choice to the empty room");
							}break;
						case 8:
							if(tictactoe[2][1]==' ' && choose==8){
								if(number==1){
									tictactoe[2][1]='x';
									number--;
								}
								else if(number==0){
									tictactoe[2][1]='o';
									number++;
								}
									if(player==1)player++;
									else if(player==2)player--;
							}
							else if(tictactoe[2][1]!=' ' && choose==8){
								System.out.println("Input your choice to the empty room");
							}break;
						case 9:
							if(tictactoe[2][2]==' ' && choose==9){
								if(number==1){
									tictactoe[2][2]='x';
									number--;
								}
								else if(number==0){
									tictactoe[2][2]='o';
									number++;
								}	
									if(player==1)player++;
									else if(player==2)player--;
							}
							else if(tictactoe[2][2]!=' ' && choose==9){
								System.out.println("Input your choice to the empty room");
							}break;
						default:
							System.out.println("Please input Appropriate choice !");
							break;
						}				
					}
					
					if(choice2 == 1 ){
						continue;
					}
					else if(choice2 == 0 ){
						break;
					}
					
				}while(true);
		}
		
		public static void main(String[] args) {
			do{
				int pilih=-1;
					
				System.out.println();
				System.out.println("===========TIC TAC TOE Game===========");
				System.out.println("1. Play");
				System.out.println("0. Exit");
				System.out.print("Input Choice : ");
				try{
					pilih=tictac.nextInt();
				}catch(InputMismatchException e){
					System.out.println("Input 1 or 0");
				}
				switch(pilih){
				case 1:
					tictac.nextLine();
					play();
					break;
				case 0:
					System.exit(0);
				default:
				 	System.out.println();
					System.out.println("Input 1 or 0");
					break;
				}
			
			}while(true);
	    }	
	}

