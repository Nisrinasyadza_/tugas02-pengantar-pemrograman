package TugasPP;
import java.util.*;
public class perhitugan_matriks {
	
	static Scanner mat=new Scanner(System.in);
	static int choice;
	static void MainMenu(){
		while(true){
			int Choose;
			System.out.println("\t\t   Matriks");
			System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");			
			System.out.println("1. Penjumlahan");
			System.out.println("2. Pengurangan");
			System.out.println("3. Perkalian");
			System.out.println("0. Keluar");
			System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");	
			System.out.print("Masukkan Pilihan : ");
			do{
				try{
					Choose=mat.nextInt();
					if(Choose!= 1 && Choose!= 2 && Choose!= 3  && Choose!= 0 ){
						System.out.println("Masukkan pilihan yang tersedia");
						continue;
					}
					break;
				}catch(InputMismatchException e){
					System.out.println("Masukkan pilihan yang tersedia");
					mat.nextLine();
					continue;
				}
			}while(true);
			if(Choose== 0){
				System.exit(0);
			}
			switch(Choose){
				case 1:
					penjumlahan();
					break;
				case 2:
					pengurangan();
					break;
				case 3:
					perkalian();
					break;	
			}	
		}
	}
	
	static void penjumlahan(){
		int Row,Column;
		do{
			System.out.println("\t\t  Penjumlahan");
			System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
			do{	
				try{				
					System.out.print("Masukkan jumlah baris matriks : ");
					Row=mat.nextInt();
						if (Row <1){
							throw new InputMismatchException();
						}
					System.out.print("Masukkan jumlah kolom matriks : ");
					Column=mat.nextInt();
						if (Column <1){
							throw new InputMismatchException();
						}
					break;
				}catch(InputMismatchException e){
					System.out.println("Masukkan angka lebih besar dari 0");
					mat.nextLine();
					continue;
				}
			}while(true);			
			double[][]Matrix1= new double[Row][Column];
			double[][]matrix2= new double[Row][Column];
			double[][]Result=new double[Row][Column];
			System.out.println("\t\t Matriks A");
			do{
				try{
					for(int IndexRow=0; IndexRow<Row;IndexRow++){
						for(int IndexColumn=0; IndexColumn<Column;IndexColumn++){
							System.out.print("Masukkan nilai baris "+(IndexRow+1)+" dan Kolom "+(IndexColumn+1)+" : ");
							Matrix1[IndexRow][IndexColumn]=mat.nextDouble();
						}
					}
					break;
				}catch(InputMismatchException e){
					System.out.println("Masukkan bilangan real");
					mat.nextLine();
					continue;
				}	
			}while(true);
			System.out.println();
			for(int IndexRow=0; IndexRow<Row;IndexRow++){
				for(int IndexColumn=0; IndexColumn<Column;IndexColumn++){
					System.out.printf("%.2f  ",Matrix1[IndexRow][IndexColumn]);
				}System.out.println();
			}
			System.out.println();
			System.out.println("\t\t Matriks B");
			do{
				try{
					for(int IndexRow=0; IndexRow<Row;IndexRow++){
						for(int IndexColumn=0; IndexColumn<Column;IndexColumn++){
							System.out.println("Masukkan nilai baris"+(IndexRow+1)+" dan Kolom "+(IndexColumn+1));
							System.out.print(">> ");
							matrix2[IndexRow][IndexColumn]=mat.nextDouble();
							Result[IndexRow][IndexColumn]=Matrix1[IndexRow][IndexColumn] + matrix2[IndexRow][IndexColumn];
						}
					}
					break;
				}catch(InputMismatchException e){
					System.out.println("Masukkan bilangan real");
					mat.nextLine();
					continue;
				}	
			}while(true);
			
			System.out.println();
			for(int IndexRow=0; IndexRow<Row;IndexRow++){
				for(int IndexColumn=0; IndexColumn<Column;IndexColumn++){
					
					System.out.printf("%.2f  ",matrix2[IndexRow][IndexColumn]);
				}System.out.println();
			}			
			System.out.println("\n");
			System.out.println("Hasil dari penjumlahan kedua matriks adalah : ");
			System.out.println();
			for(int IndexRow=0; IndexRow<Row;IndexRow++){
				for(int IndexColumn=0; IndexColumn<Column;IndexColumn++){
					
					System.out.printf("%.2f  ",Result[IndexRow][IndexColumn]);
				}System.out.println();
			}
			if(Row != Column){
				System.out.println("Matriks tersebut tidak memiliki determinan");
			}	
			else{
				
				if(Row==2 && Column==2){
					if(((Result[0][0]*Result[1][1])-(Result[0][1]*Result[1][0]))!=0){
						System.out.println("Determinan dari matriks tersebut adalah "+((Result[0][0]*Result[1][1])-(Result[0][1]*Result[1][0])));
					}
					else{
						System.out.println("Matriks tersebut merupakan matriks pencerminan");
					}
				}
				else if(Row==3 && Column==3){
					if(((matrix2[0][0]*((matrix2[1][1]*matrix2[2][2])-(matrix2[2][1]*matrix2[1][2])))
							-(matrix2[0][1]*((matrix2[1][0]*matrix2[2][2])-(matrix2[2][0]*matrix2[1][2])))
							+(matrix2[0][2]*((matrix2[1][0]*matrix2[2][1])-(matrix2[2][0]*matrix2[1][1]))))!=0){
						System.out.println("Determinan dari matriks tersebut adalah "+((matrix2[0][0]*((matrix2[1][1]*matrix2[2][2])-(matrix2[2][1]*matrix2[1][2])))
								-(matrix2[0][1]*((matrix2[1][0]*matrix2[2][2])-(matrix2[2][0]*matrix2[1][2])))
								+(matrix2[0][2]*((matrix2[1][0]*matrix2[2][1])-(matrix2[2][0]*matrix2[1][1])))));
					}
					else{
						System.out.println("Matriks tersebut merupakan matriks pencerminan");
					}
				}
			}	
			System.out.println();
			mat.nextInt();
			do{
				System.out.println("Tekan 1 Untuk ulangi dan 0 untuk keluar");
				choice=mat.nextInt();
				if (choice==0){
					 break;
				 }
				 else  if (choice==1){
					 break;
				 }
				 else{
					 System.out.println("Masukkan angka 1 atau 0");
					 continue;
				 }
			}while(true);
			 if (choice==0){
				 break;
			 }
			 else  if (choice==1){
				 continue;
			 }
		}while(true);		
	}
	
	static void pengurangan(){
		int Row,Column;
		do{
			System.out.println("\t\t  Pengurangan");
			System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");	
			do{	
				try{
					
					System.out.print("Masukkan jumlah baris matriks : ");
					Row=mat.nextInt();
						if (Row <1){
							throw new InputMismatchException();
						}
					System.out.print("Masukkan jumlah kolom matriks : ");
					Column=mat.nextInt();
						if (Column <1){
							throw new InputMismatchException();
						}
					break;
				}catch(InputMismatchException e){
					System.out.println("Masukkan angka lebih besar dari 0");
					mat.nextLine();
					continue;
				}
			}while(true);	
			double[][]matrix1= new double[Row][Column];
			double[][]matrix2= new double[Row][Column];
			double[][]result=new double[Row][Column];
			System.out.println("\t\t Matriks A");
			do{
				try{
					for(int indexRow=0; indexRow<Row;indexRow++){
						for(int IndexColumn=0; IndexColumn<Column;IndexColumn++){
							System.out.println("Masukkan nilai baris "+(indexRow+1)+" dan Kolom "+(IndexColumn+1));
							System.out.print(">> ");
							matrix1[indexRow][IndexColumn]=mat.nextDouble();
						}
					}
					break;
				}catch(InputMismatchException e){
					System.out.println("Masukkan bilangan real");
					mat.nextLine();
					continue;
				}	
			}while(true);			
			System.out.println();
			for(int IndexRow=0; IndexRow<Row;IndexRow++){
				for(int IndexColumn=0; IndexColumn<Column;IndexColumn++){
					
					System.out.printf("%.2f  ",matrix1[IndexRow][IndexColumn]);
				}System.out.println();
			}	
			System.out.println();		
			System.out.println("\t\t Matriks B");
			do{
				try{
					for(int IndexRow=0; IndexRow<Row;IndexRow++){
						for(int IndexColumn=0; IndexColumn<Column;IndexColumn++){
							System.out.println("Masukkan nilai baris "+(IndexRow+1)+" dan Kolom "+(IndexColumn+1));
							System.out.print(">> ");
							matrix2[IndexRow][IndexColumn]=mat.nextDouble();
							result[IndexRow][IndexColumn]= matrix1[IndexRow][IndexColumn] - matrix2[IndexRow][IndexColumn];
						}
					}
					break;
				}catch(InputMismatchException e){
					System.out.println("Masukkan bilangan real");
					mat.nextLine();
					continue;
				}	
			}while(true);		
			System.out.println();
			for(int IndexRow=0; IndexRow<Row;IndexRow++){
				for(int IndexColumn=0; IndexColumn<Column;IndexColumn++){
					
					System.out.printf("%.2f  ",matrix2[IndexRow][IndexColumn]);
				}System.out.println();
			}		
			System.out.println("\n");
			System.out.println("Hasil pengurangan kedua matriks tersebut adalah : ");
			System.out.println();	
			for(int IndexCoumn=0; IndexCoumn<Row;IndexCoumn++){
				for(int IndexColumn=0; IndexColumn<Column;IndexColumn++){
					
					System.out.printf("%.2f  ",result[IndexCoumn][IndexColumn]);
				}System.out.println();
			}
			if(Row != Column){
				System.out.println("Matriks tersebut tidak memiliki determinan");
			}	
			else{
				
				if(Row==2 && Column==2){
					if(((result[0][0]*result[1][1])-(result[0][1]*result[1][0]))!=0){
						System.out.println("Determinan dari matriks tersebut adalah "+((result[0][0]*result[1][1])-(result[0][1]*result[1][0])));
					}
					else{
						System.out.println("Matriks tersebut merupakan matriks pencerminan");
					}
				}
				else if(Row==3 && Column==3){
					if(((matrix2[0][0]*((matrix2[1][1]*matrix2[2][2])-(matrix2[2][1]*matrix2[1][2])))
							-(matrix2[0][1]*((matrix2[1][0]*matrix2[2][2])-(matrix2[2][0]*matrix2[1][2])))
							+(matrix2[0][2]*((matrix2[1][0]*matrix2[2][1])-(matrix2[2][0]*matrix2[1][1]))))!=0){
						System.out.println("Determinan dari matriks tersebut adalah "+((matrix2[0][0]*((matrix2[1][1]*matrix2[2][2])-(matrix2[2][1]*matrix2[1][2])))
								-(matrix2[0][1]*((matrix2[1][0]*matrix2[2][2])-(matrix2[2][0]*matrix2[1][2])))
								+(matrix2[0][2]*((matrix2[1][0]*matrix2[2][1])-(matrix2[2][0]*matrix2[1][1])))));
					}
					else{
						System.out.println("Matriks tersebut merupakan matriks pencerminan");
					}
				}
			}
			System.out.println();
			mat.nextLine();
			do{
				System.out.println("Tekan 1 untuk ulangi dan 0 untuk keluar");
				choice=mat.nextInt();
				if (choice==0){
					 break;
				 }
				 else  if (choice==1){
					 break;
				 }
				 else{
					 System.out.println("Masukkan angka 1 atau 0");
					 continue;
				 }
			}while(true);
			
				 if (choice==0){
					 break;
				 }
				 else  if (choice==1){
					 continue;
				 }		
		}while(true);	
	}
	
	static void perkalian(){	
		while(true){
			 int matriksMultiTotal=0,choice=0,Row1,Column1,Row2,Column2;
		 do{
			 do{
				 try{
					 System.out.print("Jumlah baris matriks A: ");
					 Row1=mat.nextInt();
					 System.out.print("Jumlah kolom matriks A: ");
					 Column1=mat.nextInt();
					 System.out.print("Jumlah baris matriks B: ");
					 Row2=mat.nextInt();
					 System.out.print("Jumlah kolom matriks B: ");
					 Column2=mat.nextInt();
					 break;
				 }catch(InputMismatchException e){
					 System.out.println("Masukkan bilangan bulat");
					 mat.nextLine();
					 continue;
				 }
			 }while(true); 
					if (Column1 != Row2) {
						System.out.println("Matriks tidak dapat dikalikan");					
						do{
							try{
								System.out.print("Tekan 1 untuk ulangi dan 0 untuk keluar : ");
								choice=mat.nextInt();
								if (choice==0)break;
								if (choice==1)break;
							}catch(InputMismatchException e){
								System.out.println("Masukkan pilihan yang tersedia");
								mat.nextLine();
								continue;
							}
						}while((choice!=0)||(choice!=1));
					}			
					if (choice==0)break;
					if (choice==1)continue;
		 }while(Column1!=Row2);
			 if (choice==1)break;
			 System.out.println("\n"); 
		int[][]matriks=new int[Row1][Column1];
		do{
			System.out.println("Matriks A: ");
			for(int indeksbaris=0;indeksbaris<Row1;indeksbaris++){
				for(int indekskolom=0;indekskolom<Column1;indekskolom++){
					System.out.print("Baris "+(indeksbaris+1)+" Kolom "+(indekskolom+1)+" : ");
					try{
						matriks[indeksbaris][indekskolom]=mat.nextInt();
					}catch(InputMismatchException e){
						System.out.println("Masukkan bilangan bulat");
						mat.nextLine();
						indekskolom--;
						continue;
					}	
				}
			}
			break;
		}while(true);		
		System.out.println();	
		for(int indeksbaris=0;indeksbaris<Row1;indeksbaris++){
			for(int indekskolom=0;indekskolom<Column1;indekskolom++){
				System.out.printf("%4d ", matriks[indeksbaris][indekskolom]);
			}System.out.println();
		}System.out.println();
		int[][]matrix2=new int[Row2][Column2];
		do{
			System.out.println("Matriks B: ");
			for(int indeksbaris=0;indeksbaris<Row2;indeksbaris++){
				for(int indekskolom=0;indekskolom<Column2;indekskolom++){
					System.out.print("row "+(indeksbaris+1)+" Kolom "+(indekskolom+1)+" : ");
					try{
						matrix2[indeksbaris][indekskolom]=mat.nextInt();
					}catch(InputMismatchException e){
						System.out.println("Masukkan bilangan bulat");
						mat.nextLine();
						indekskolom--;
						continue;
					}
				}
			}
			break;
		}while(true);	
		System.out.println();	
		for(int IndexRow=0;IndexRow<Row2;IndexRow++){
			for(int IndexColumn=0;IndexColumn<Column2;IndexColumn++){
				System.out.printf("%4d ", matrix2[IndexRow][IndexColumn]);
			}System.out.println();
		}	
		int[][]matrix3=new int[Row1][Column2];
		for(int IndexRow=0;IndexRow<Row1;IndexRow++){
			for(int IndexColumn=0;IndexColumn<Column2;IndexColumn++){
				matriksMultiTotal=0;
				for(int perkalianindex=0;perkalianindex<Column1;perkalianindex++){
					matriksMultiTotal+=matriks[IndexRow][perkalianindex]*matrix2[perkalianindex][IndexColumn];
				}
				matrix3[IndexRow][IndexColumn]=matriksMultiTotal;
					}
				}System.out.println();		
		System.out.println("Perkalian dari matriks "+Row1+"x"+Column1+" X "+Row2+"x"+Column2+" Akan membuat matriks "+Row1+"x"+Column2);
		System.out.println("hasil dari perkalian kedua matriks adalah : ");
		for(int IndeksRow=0;IndeksRow<Row1;IndeksRow++){
			for(int IndexColumn=0;IndexColumn<Column2;IndexColumn++){
				System.out.printf("%4d ", matrix3[IndeksRow][IndexColumn]);
			}
			System.out.println();
		}		
		System.out.println("\n");		
		if(Row1 != Column2){
			System.out.println("Matriks tersebut tidak memiliki determinan");
		}	
		else{
			if(Row1==2 && Column2==2){
				if(((matrix3[0][0]*matrix3[1][1])-(matrix3[0][1]*matrix3[1][0]))!=0){
					System.out.println("Determinan dari matriks tersebut adalah "+((matrix3[0][0]*matrix3[1][1])-(matrix3[0][1]*matrix3[1][0])));
				}
				else{
					System.out.println("Matriks tersebut merupakan matriks pencerminan");
				}
			}
			else if(Row1==3 && Column2==3){
				if(((matrix2[0][0]*((matrix2[1][1]*matrix2[2][2])-(matrix2[2][1]*matrix2[1][2])))
						-(matrix2[0][1]*((matrix2[1][0]*matrix2[2][2])-(matrix2[2][0]*matrix2[1][2])))
						+(matrix2[0][2]*((matrix2[1][0]*matrix2[2][1])-(matrix2[2][0]*matrix2[1][1]))))!=0){
					
					System.out.println("Determinan dari matriks tersebut adalah "+((matrix2[0][0]*((matrix2[1][1]*matrix2[2][2])-(matrix2[2][1]*matrix2[1][2])))
							-(matrix2[0][1]*((matrix2[1][0]*matrix2[2][2])-(matrix2[2][0]*matrix2[1][2])))
							+(matrix2[0][2]*((matrix2[1][0]*matrix2[2][1])-(matrix2[2][0]*matrix2[1][1])))));
				}
				else{
					System.out.println("Matriks tersebut merupakan matriks pencerminan");
				}
			}
		}		
			do{
				try{
					System.out.print("Tekan 1 untuk ulangi dan 0 untuk keluar : ");
					choice=mat.nextInt();
					if (choice==0)break;
					if (choice==1)break;
				}catch(InputMismatchException e){
					System.out.println("Masukkan pilihan yang tersedia");
					mat.nextLine();
					continue;
				}		
			}while((choice<0)||(choice>1));
			if (choice==0)break;
			if(choice==1)continue;
			System.out.println();
		 }
	}
	
	public static void main(String[] args){
			MainMenu();
    }	
}


