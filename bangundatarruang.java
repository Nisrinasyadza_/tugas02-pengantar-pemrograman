package TugasPP;
import java.util.*;
	public class bangundatarruang {

		public static void menu(){
			// TODO Auto-generated method stub
			System.out.println("1. Bangun datar");
			System.out.println("2. Bangun ruang");
			System.out.println("3. Keluar");
			System.out.println(" Pilih operasi perhitungan : ");
		}
		public static void Bangun_datar(){
			int pilihan = -1;
			System.out.println("1. Persegi");
			System.out.println("2. Persegi panjang");
			System.out.println("3. Segitiga");
			System.out.println("4. Jajar genjang");
			System.out.println("5. Belah ketupat");
			System.out.println("6. Trapesium");
			System.out.println("7. Lingkaran");
			System.out.println(" Pilih operasi perhitungan : ");
			Scanner input = new Scanner(System.in);
			
			do{
				try{
					pilihan=input.nextInt();
					continue;
				}
				catch(InputMismatchException e){
					System.out.println("Salah! input sekali lagi!");
					input.nextLine();
					continue;
				}
			}while(pilihan<1 || pilihan>7);
			switch (pilihan){
			case 1 :
				Square();
				break;
			case 2 :
				Rectangle();
				break;
			case 3 :
				Triangle();
				break;
			case 4 :
				Parallelogram();
				break;
			case 5 :
				Rhombus();
				break;
			case 6 :
				Trapezoid();
				break;
			case 7 :
				Circle();
				break;
			}
		}
		public static void Square(){
			Scanner input = new Scanner(System.in);
			System.out.println("Masukkan Panjang Sisi :");
			float side;
			do{
				try{
					side=input.nextFloat();
					break;
				}
				catch(InputMismatchException e){
					System.out.println("Salah, silahkan input sekali lagi !");
					input.nextLine();
					continue;
				}
			}while(true);
			double SquareArea=side*side;
			System.out.println("Luas persegi : "+SquareArea);
			double SquarePerimeter = 4*side;
			System.out.println("Keliling persegi : "+SquarePerimeter);
		}
		public static void Rectangle(){
			Scanner input = new Scanner(System.in);
			System.out.println("Masukkan Panjang : ");
			float lenght;
			System.out.println("Masukkan Lebar : ");
			float width;
			do{
				try{
					lenght=input.nextFloat();
					width=input.nextFloat();
					break;
				}
				catch(InputMismatchException e){
					System.out.println("Salah! input sekali lagi!");
					input.nextLine();
					continue;
				}
			}while(true);
			double RectangleArea=lenght*width;
			System.out.println("Luas Persegi Panjang : "+RectangleArea);
			double RectanglePerimeter=(2*lenght)+(2*width);
			System.out.println("Keliling persegi panjang adalah "+RectanglePerimeter);
		}
		public static void Triangle(){
			Scanner input = new Scanner (System.in);
			System.out.println("Masukkan sisi alas : ");
			float base;
			System.out.println("Masukkan sisi miring : ");
			float side;
			System.out.println("Masukkan tinggi : ");
			float height;
			do{
				try{
					base=input.nextFloat();
					side=input.nextFloat();
					height=input.nextFloat();
					break;
				}
				catch(InputMismatchException e){
					System.out.println("Salah, silahkan input sekali lagi");
					input.nextLine();
					continue;
				}
			}while(true);
			double TriangleArea=1/2*(base*height);
			System.out.println("Luas Segitiga adalah "+TriangleArea);
			double TrianglePerimeter=base+(2*side);
			System.out.println("Keliling Segitiga adalah "+TrianglePerimeter);
		}
		public static void Parallelogram(){
			Scanner input = new Scanner (System.in);
			System.out.println("Masukkan alas : ");
			float lenght;
			System.out.println("Masukkan tinggi : ");
			float height;
			System.out.println("Masukkan sisi miring : ");
			float side;
			do{
				try{
					lenght=input.nextFloat();
					height=input.nextFloat();
					side=input.nextFloat();
					break;
				}
				catch(InputMismatchException e){
					System.out.println("Salah! Input sekali lagi !");
					input.nextLine();
					continue;
				}
			}while(true);
			double ParallelogramArea=lenght*height;
			System.out.println("Luas Jajar Genjang adalah "+ParallelogramArea);
			double ParallelogramPerimeter=(2*lenght)+(2*side);
			System.out.println("Keliling Jajar Genjang adalah "+ParallelogramPerimeter);
		}
		public static void Rhombus(){
			Scanner input = new Scanner (System.in);
			System.out.println("Masukkan diagonal 1 : ");
			float diagonal1;
			System.out.println("Masukkan diagonal 2 : ");
			float diagonal2;
			System.out.println("Masukkan sisi :");
			float side;
			do{
				try{
					diagonal1=input.nextFloat();
					diagonal2=input.nextFloat();
					side=input.nextFloat();
					break;
				}
				catch(InputMismatchException e){
					System.out.println("Salah! Input sekali lagi!");
					input.nextLine();
					continue;
				}
			}while(true);
			double RhombusArea=1/2*(diagonal1*diagonal2);
			System.out.println("Luas Persegi Panjang adalah "+RhombusArea);
			double RhombusPerimeter= 4*side;
			System.out.println("Keliling Persegi Ketupat adalah "+RhombusPerimeter);
		}
		public static void Trapezoid(){
			Scanner input = new Scanner (System.in);
			System.out.println("Masukkan Panjang sisi pertama (sisi sejajar) : alas : ");
			float side1;
			System.out.println("Masukkan panjang sisi kedua (sisi sejajar) : ");
			float side2;
			System.out.println("Masukkan panjang sisi ketiga : ");
			float side3;
			System.out.println("Masukkan panjang sisi keempat : ");
			float side4;
			System.out.println("Masukkan tinggi trapesium : ");
			float height;
			do{
				try{
					side1=input.nextFloat();
					side2=input.nextFloat();
					side3=input.nextFloat();
					side4=input.nextFloat();
					height=input.nextFloat();
					break;
				}
				catch(InputMismatchException e){
					System.out.println("Salah! Input sekali lagi!");
					input.nextLine();
					continue;
				}
			}while(true);
			double area = 0.5*(side1+side2)*height;
			System.out.println("Luas Trapesium : "+area);
			double TrpezoidPerimeter = side1+side2+side3+side4;
			System.out.println("Keliling Trapesium : "+TrpezoidPerimeter);
		}
		public static void Circle(){
			Scanner input = new Scanner (System.in);
			System.out.println("Masukkan Panjang Jari-jari Lingkaran : ");
			float radius;
			do{
				try{
					radius=input.nextFloat();
					break;
				}
				catch(InputMismatchException e){
					System.out.println("Salah! Input sekali lagi!");
					input.nextLine();
					continue;
				}
			}while(true);
			double Area = Math.PI*Math.pow(radius, 2);
			System.out.println("Luas Lingkaran "+Area);
			double circumfrence = 2*Math.PI*radius;
			System.out.println("Keliling Lingkaran adalah "+circumfrence);
				}
				public static void Bangun_ruang(){
					int choice = -1;
					System.out.println("1. Tabung");
					System.out.println("2. Balok");
					System.out.println("3. Kubus");
					System.out.println("4. Kerucut");
					System.out.println("5. Bola");
					System.out.println("6. Limas Segiempat");
					System.out.println(" Silahkan pilih operasi perhitungan yang diinginkan ");
					Scanner input = new Scanner(System.in);
					
					do{
						try{
							choice=input.nextInt();
							continue;
						}
						catch(InputMismatchException e){
							System.out.println("Salah! Input sekali lagi");
							input.nextLine();
							continue;
						}
					}while(choice<1 || choice>7);
					switch (choice){
					case 1 :
						Cylinder();
						break;
					case 2 :
						Cuboid();
						break;
					case 3 :
						Cube();
						break;
					case 4 :
						Cone();
						break;
					case 5 :
						Sphered();
						break;
					case 6 :
						Pyramid();
						break;
					}
					
				}
				public static void Cylinder(){
					Scanner input = new Scanner (System.in);
					System.out.println("Masukkan Jari-jari : ");
					float radius;
					System.out.println("Masukkan Tinggi Tabung : ");
					float height;
					do{
						try{
							radius=input.nextFloat();
							height=input.nextFloat();
							break;
						}
						catch(InputMismatchException e){
							System.out.println("Salah! Input sekali lagi!");
							input.nextLine();
							continue;
						}
					}while(true);
					double Area = 2*Math.PI*radius*(radius+height);
					System.out.println("Luas Tabung "+Area);
					double Volume = Math.PI*Math.pow(radius,2)*height;
					System.out.println("Volume tabung adalah "+Volume);
				}
					public static void Cuboid(){
						Scanner input = new Scanner (System.in);
						System.out.println("Masukkan Panjang Balok : ");
						float lenght;
						System.out.println("Masukkan lebar balok : ");
						float width;
						System.out.println("Masukkan tinggi balok : ");
						float height;

						do{
							try{
								lenght=input.nextFloat();
								width=input.nextFloat();
								height=input.nextFloat();
								break;
							}
							catch(InputMismatchException e){
								System.out.println("Salah! Input sekali lagi!");
								input.nextLine();
								continue;
							}
						}while(true);
						double Area = 2*((lenght*width)+(lenght*height)+(width+height));
						System.out.println("Luas balok "+Area);
						double CoboidPerimeter = 4*(lenght+width+height);
						System.out.println("keliling permukaan balok adalah "+CoboidPerimeter);
						double Volume = lenght*width*height;
						System.out.println("Volume balok adalah "+Volume);
					}
					public static void Cube(){
						Scanner input = new Scanner (System.in);
						System.out.println("Masukkan Panjang sisi Balok : ");
						float side;

						do{
							try{
								side=input.nextFloat();
								break;
							}
							catch(InputMismatchException e){
								System.out.println("Salah, silahkan input sekali lagi");
									input.nextLine();
				continue;
							}
						}while(true);
						double Area = 6*Math.pow(side,2);
						System.out.println("Luas permukaan balok"+Area);
						double perimeter = 12*side;
						System.out.println("keliling permukaan balok adalah "+perimeter);
						double Volume = Math.pow(side,3);
						System.out.println("Volume balok adalah "+Volume);
					}
					public static void Cone(){
						Scanner input = new Scanner (System.in);
						System.out.println("Masukkan jari - jari : ");
						float radius;
						System.out.println("Masukkan tinggi kerucut : ");
						float height;

						do{
							try{
								radius=input.nextFloat();
								height=input.nextFloat();
								break;
							}
							catch(InputMismatchException e){
								System.out.println("Salah! Input sekali lagi!");
									input.nextLine();
				continue;
							}
						}while(true);
						double area = Math.PI*radius*(radius+Math.sqrt(Math.pow(height,2))+Math.pow(radius, 2));
						System.out.println("Luas kerucut"+area);
						double Volume = 1/3*Math.PI*Math.pow(radius,2)*height;
						System.out.println("Volume kerucut adalah "+Volume);					
					}
					public static void Sphered(){
						Scanner input = new Scanner (System.in);
						System.out.println("Masukkan jari - jari : ");
						float radius;

						do{
							try{
								radius=input.nextFloat();
								break;
							}
							catch(InputMismatchException e){
								System.out.println("Salah, silahkan input sekali lagi");
									input.nextLine();
				continue;
							}
						}while(true);
						double area = 4*Math.PI*Math.pow(radius,2);
						System.out.println("Luas bola"+area);
						double Volume = 3/4*Math.PI*Math.pow(radius,3);
						System.out.println("Volume bola adalah "+Volume);	
										
						}
						public static void Pyramid(){
							Scanner input = new Scanner (System.in);
							System.out.println("Masukkan Panjang sisi alas : ");
							float side;
							System.out.println("Masukkan Tinggi limas : ");
							float height;
							do{
								try{
									side=input.nextFloat();
									height=input.nextFloat();
									break;
								}
								catch(InputMismatchException e){
									System.out.println("Salah! Input sekali lagi!");
										input.nextLine();
					continue;
								}
							}while(true);
							double BaseArea = Math.pow(side,2);
							double HeightSide = Math.sqrt(Math.pow(0.5*side,2));
							double SideArea = 0.5*side*HeightSide;
							double Luas = BaseArea+(4*SideArea);
							System.out.println("Luas Limas Segiempat"+Luas);
							double perimeter = (4*side)+(4*Math.sqrt(Math.pow(HeightSide,2)+Math.pow(0.5*side,2)));
							System.out.println("Keliling Limas Segiempat"+perimeter);
							double Volume = 1/3*BaseArea*height;
							System.out.println("Volume Limas Segiempat"+Volume);
							
						}
						public static void main (String[]args){
							int choice = -1;
									Scanner input = new Scanner(System.in);
									menu();
									do{
										try{
										choice = input.nextInt();
										break;
										}catch(InputMismatchException e){
											System.out.println("Salah, silahkan input sekali lagi");
											input.nextLine();
											continue;
										}
									}while(true);
									switch(choice){
									case 1:
										Bangun_datar();
										break;
									case 2:
										Bangun_ruang();
										break;	
									}
											
				}
			}



